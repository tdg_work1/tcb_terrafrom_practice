#!/bin/bash
sudo yum update -y && sudo yum install -y docker
curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
sudo yum install -y nodejs
sudo systemctl start docker
sudo usermod -aG docker ec2-user
docker run --name mynginx1 -p 80:80 -d nginx