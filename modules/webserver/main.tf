data "aws_ami" "latest-amazon-linux-image" {
most_recent = true
owners = ["amazon"]
filter {
name = "name"
values = [var.image_name]
}
filter {
name = "virtualization-type"
values = ["hvm"]
}
}

resource "aws_instance" "web" {
  ami=data.aws_ami.latest-amazon-linux-image.id
  instance_type=var.instance_type
  tags = {
    Name= "Terraform Ec2"
  
  }

  key_name = aws_key_pair.ssh-key.key_name

  user_data = file("entry-script.sh")
}

resource "tls_private_key" "example_keypair" {

algorithm = "RSA"

rsa_bits = 2048

}


resource "aws_default_security_group" "sec_group" {
  
  vpc_id =var.vpc_id

  ingress{
    from_port =22
    to_port=22
    protocol="tcp"
    cidr_blocks=[var.my_ip]
  }

  
  ingress{
    from_port =8080
    to_port=8080
    protocol="tcp"
    cidr_blocks=["0.0.0.0/0"]
  }

   egress{
    from_port =0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
    prefix_list_ids=[]
  }
}



resource "aws_key_pair" "ssh-key" {

key_name = "server-key"

public_key = tls_private_key.example_keypair.public_key_openssh

}



resource "aws_security_group" "sec_group_test" {
  
  name = "Hello"


  
  ingress{
    from_port =0
    to_port=0
    protocol="all"
    cidr_blocks=["0.0.0.0/0"]
  }

   egress{
    from_port =0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
    prefix_list_ids=[]
  }
}